# Story and Image Generation Studio

Welcome to the Story and Image Generation Studio! This app provides a creative platform where users can generate unique stories based on their input prompts. It pairs the story with a relevant image fetched using the Google Custom Search API, providing a visual complement to the generated narrative.

Video demo: https://www.youtube.com/watch?v=1umVqGR0c2Q&ab_channel=SuzannaThompson

## Overview

This application uses the GPT-2 model from OpenAI's Transformers library for text generation. GPT-2 is a state-of-the-art language model that uses deep learning to produce human-like text. The app takes a user-provided prompt and generates a story, showcasing the capabilities of language models in creative writing.

## Features

- **Text Generation**: Leveraging the power of GPT-2 to create stories from user prompts.
- **Image Search**: Integrating Google Custom Search to find and display an image related to the story.
- **Streamlit Interface**: A clean and intuitive UI designed with Streamlit for easy interaction.

## Usage

To get started with the Story and Image Generation Studio:

1. Clone the repository to your local machine.
2. Install the required dependencies using `pip install -r requirements.txt`.
3. Run the Streamlit app with `streamlit run app.py`.

**Note: you will need to generate an API key for the google custom search API. Learn more here: https://developers.google.com/custom-search/v1/overview **

Enter a prompt into the text box and click the 'Generate Story and Image' button. The app will display a generated story along with an image.

## Model Information

The app uses the GPT-2 model, known for its ability to generate coherent and contextually relevant text. However, GPT-2's extensive capabilities can sometimes lead to increased response times, which may affect the overall performance of the app. Future iterations may consider using more efficient models or optimizing the current setup to enhance responsiveness.

## Process

The application follows these steps to generate content:

1. **Prompt Input**: Users input their story prompt in the provided text field.
2. **Story Generation**: Upon submission, the app uses GPT-2 to generate a story based on the input prompt.
3. **Image Retrieval**: Simultaneously, the app performs an image search using the Google Custom Search API to find a picture that matches the story theme.
4. **Display**: Both the generated text and image are displayed on the UI for the user to read and visualize the story.

## Note on Performance

The GPT-2 model is a powerful language generation tool, but it can be resource-intensive. Users might experience slower response times due to the complexity of the model, especially when generating longer pieces of text. Optimizations and performance improvements are considerations for future development.



